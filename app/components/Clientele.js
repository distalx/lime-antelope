import React from 'react';

class Clientele extends React.Component {
  getClientele() {
    return [
      {
        "name": "Metcalf Foundation",
        "url": "http://metcalffoundation.com/",
        "logoSrc": "images/logo-metcalf-foundation.png"
      },
      {
        "name": "Ontario Trillium Foundation",
        "url": "http://www.otf.ca/",
        "logoSrc": "images/logo-ontario-trillium.jpg"
      },
      {
        "name": "Saskatchewan Arts Board",
        "url": "http://www.saskartsboard.com/",
        "logoSrc": "images/logo-sask-arts-board.jpg"
      },
      {
        "name": "RCDC",
        "url": "http://rcdc.ca/",
        "logoSrc": "images/logo-RCDC.png"
      },
      {
        "name": "CLEO",
        "url": "http://cleo.on.ca/",
        "logoSrc": "images/logo-cleo.gif"
      },
      {
        "name": "The Stop",
        "url": "http://thestop.org/",
        "logoSrc": "images/logo-stop.png"
      },
      {
        "name": "Equestrian Canada",
        "url": "http://equestrian.ca",
        "logoSrc": "images/logo-ec.png"
      },
      {
        "name": "Jane's Walk",
        "url": "http://janeswalk.org/",
        "logoSrc": "images/logo-janes.jpg"
      },
      {
        "name": "Ontario Justice Education Network",
        "url": "http://www.ojen.ca/",
        "logoSrc": "images/logo-ojen.png"
      }
    ];

  }
  renderClientele() {
    return this.getClientele().map((c) => {
      return(<div  key={c.url}>
        <h4>{c.name}</h4>
      </div>);
    });
  }
  render() {
    return (<div>
      Clientele !

      {this.renderClientele()}
    </div>);
  }
}

export default Clientele;
