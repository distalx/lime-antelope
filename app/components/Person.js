import React from 'react';

import CTA from './CTA';

class Person extends React.Component {
  getPersonInfo(personId) {
    let people = [
      {
        "id": "daniel",
        "coverImage": "/images/team-daniel.jpg",
        "name": "Daniel Bernhard",
        "title": "President",
        "intro": "Daniel leads the Mushroom Cloud team, overseeing and participating in almost all activities, from coding to modelling to community facilitation.",
        "bio": [
          "Since launching the business in 2010, Daniel has shepherded numerous projects to completion, serving clients in five countries.",
          "Previously, Daniel was a Special Projects Adviser to the mergers and acquisitions practice at Kaiser Associates, a mid-size research and strategy consulting firm with offices in six countries. Prior to that, Daniel worked as a Community Development Specialist for Spice Innovations, a volunteer management and community engagement consultancy serving non-profit organizations and public sector bodies in England, Wales, and Kenya."
        ],
        "education": [
          "MPhil, University of Cambridge",
          "BSc. (1st Class) London School of Economics",
          "SROI Certificate, New Economics Foundation"
        ],
        "humanLanguages": [
          "English – Native",
          "French – Fluent",
          "Spanish – Fluent"
        ],
        "computerLanguages": [
          "HTML, CSS, JS – Fluent",
          "Meteor.js, Node.js – Fluent"
        ]
      }, {
        "id": "eva",
        "coverImage": "/images/team-eva.jpg",
        "name": "Dr. Eva Majernikova",
        "title": "Research",
        "intro": "A talented researcher and policy maker, Eva’s core area of expertise lies in sector-level best practices research. She has conducted a number of large-scale comparative investigations both in Canada and internationally, covering a broad array of subjects including cluster-based management, robotics, and clean-tech, among others.",
        "bio": [
          "Eva’s impressive CV includes stints at the Toronto Board of Trade, the UK Foreign and Commonwealth Office, the American Chamber of Commerce (Slovakia), the European Commission (Slovak delegation),  and the United Nations Development Program.",
          "Eva’s enjoys a deep connection to the public benefit sector. She earned her PhD for her work comparing the performance evaluation and impact assessment practices of nonprofits in the OECD countries."
        ],
        "education": [
          "PhD International Economic Relations, University of Economics, Bratislava, Slovakia",
          "MSc. International Economic Relations, University of Economics, Bratislava, Slovakia",
          "Diploma in International Economic and Political Studies, Georgetown University, Washington, DC"
        ],
        "humanLanguages": [
          "Slovak – Native",
          "English – Fluent",
          "Czech – Advanced",
          "German – Advanced",
          "French – Beginner"
        ],
        "computerLanguages": [
          "HTML, CSS, JS – Fluent",
          "Meteor.js, Node.js – Fluent"
        ]
      }, {
        "id": "george",
        "coverImage": "/images/team-george.jpg",
        "name": "Dr. George Carothers",
        "title": "Research & Engagement",
        "intro": "George supports the research and engagement aspects of our practice, which encompasses best practices benchmarking, environmental scans, and direct stakeholder engagement. He’s a go-getter, a get’r done’r, and a strong contributor to the Mushroom Cloud team.",
        "bio": [
          "George is a talented social scientist focused on growth and equity. He recently completed his PhD at the university of Cambridge, where he studied life in the tech parks that have emerged to serve India’s booming hi-tech industries. His extensive research experience and interests include urban studies, public policy, and competitiveness."
        ],
        "education": [
          "PhD, University of Cambridge",
          "MSc in International Planning (Sustainable Development), University College London (UCL), UK",
          "BEs (honours), Planning, University of Waterloo, Canada"
        ],
        "humanLanguages": [

        ],
        "computerLanguages": [

        ]
      }, {
        "id": "greg",
        "coverImage": "/images/team-greg.jpg",
        "name": "Dr. Gregory Hall",
        "title": "Research & Engagement",
        "intro": "Strategy &amp; Operations",
        "bio": [
          "Greg's extensive experience in the retail, health, and development sectors and his top-tier consulting background (HIO Group, KPMG) are valuable additions to our skill set."
        ],
        "education": [
          "Lean Green Belt Certification, KPMG Lean Institute",
          "EMBA, Richard Ivey School Of Business, University of Western Ontario",
          "MBBS Honours, Bachelor of Medicine and Bachelor of Surgery, University of Sydney, Australia",
          "BSc, Specialized Honours Program in Kinesiology and Health Sciences, York University, Toronto"
        ],
        "humanLanguages": [],
        "computerLanguages": []
      }, {
        "id": "JW",
        "coverImage": "",
        "name": "Dr. JW (aka Jan-Willem van de Meent)",
        "title": "Data Master",
        "intro": "JW advises Mushroom Cloud on data science matters. His day job is Professor of Statistics and Computer Science at Northeastern University in Boston, Massachusetts.",
        "bio": [
          "JW is a world expert in the subjects of machine learning and artificial intelligence, and has years of experience doing down and dirty data analysis in various scientific disciplines. He knows when simpler is better, but also has the chops to do the hard math, when required.",
          "He previously worked at Oxford University, where he designed new programming languages that make machine learning tasks a breeze. This posting followed a grand tour of the academic circuit, with stops at MIT, Columbia, and The University of Cambridge."
        ],
        "education": [
          "PhD, Leiden / Cambridge",
          "MSc. (Cum Laude), Leiden"
        ],
        "humanLanguages": [
          "English – Annoyingly perfect for a non-native speaker",
          "Dutch – Dusty but trusty",
          "French – Pas mal du tout"
        ],
        "computerLanguages": [
          "Python – Fluent",
          "Java, Clojure – Fluent",
          "Anglican – Creator",
          "Matlab – Fluent",
          "Scala, C, C#, F# – Conversational"
        ]
      }, {
        "id": "oni",
        "coverImage": "/images/team-oni.jpg",
        "name": "Oni Ornan",
        "title": "Quantitative Analysis",
        "intro": "Oni provides quantitative analysis and research support to all aspects of our work.",
        "bio": [
          "Oni is a recent graduate of U of T's Engineering Science Program, where he took a specialization in Mathematics, Statistics, and Finance and graduated with High Honours. His immense quantitative abilities make a strong contribution to our overall skillset. Models he built before joining our team are currently informing millions of dollars of operations in a Big Five bank, major utilities, and merchant bank."
        ],
        "education": [
          "BASc. Engineering Science (High Honours), University of Toronto"
        ],
        "humanLanguages": [
          "Hebrew – Native",
          "English – Fluent",
          "Arabic – Conversational"
        ],
        "computerLanguages": [
          "R – Fluent",
          "Matlab – Fluent",
          "VBA – Fluent",
          "Python – Fluent"
        ]
      }, {
        "id": "zack",
        "coverImage": "",
        "name": "Zack Newsham",
        "title": "Technology",
        "intro": "Zack leads the Mushroom Cloud technology team, where he supervises all things computer.",
        "bio": [
          "Zack has developed numerous web-based and desktop software applications, utilizing a number of technologies and frameworks (PHP, Java, .NET, Node.js, Google App Engine, Python, etc.). He also has commercial experience developing safety-critical systems such as those found in trains, jetliners, and cars.",
          "Zack's academic expertise lies in the area of computer security and encryption, as well as systems reliability. When he can be torn from his computer, Zack heads straight for the nearest rock face, where he can climb to his heart's content."
        ],
        "education": [
          "MASc Electrical and Computer Engineering, University of Waterloo",
          "BSc. Hons. (1st Class), University of Glamorgan"
        ],
        "humanLanguages": [
          "British – Native",
          "English – Decent"
        ],
        "computerLanguages": [
          "PHP – Native",
          "Java – Fluent",
          "Meteor.js, Node.js – Fluent",
          "Python – Advanced"
        ]
      }
    ];

    return people.find((person)=>{
      return person.id === personId;
    })
  }
  renderPerson(personId) {
    let person = this.getPersonInfo(personId)


    return (<div>
      <h3>{person.name}</h3>
      <h4>{person.title}</h4>
      <h5>{person.intro}</h5>
    </div>)
  }
  render() {
    return (<div>
      <h4>Person</h4>
      {this.renderPerson(this.props.params.personId)}
      <hr/>
      <CTA/>
    </div>);
  }
}

export default Person;
