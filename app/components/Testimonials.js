import  React from 'react';

import Testimonial from './Testimonial';

class Testimonials extends React.Component {
  getTestimonials() {
    return [
      {
        "quote": "Mushroom Cloud was incredibly helpful – helping us envision solutions that we hadn’t even imagined, working with partners around the globe to meet our unusual needs, and delivering the project on time and budget. We couldn’t have been more pleased.",
        "caption": "Michael Jones - CEO, Saskatchewan Arts Board",
        "clientName": "saskBoard"
      },
      {
        "quote": "Mushroom Cloud quickly learned our business and offered innovative solutions that significantly improved our processes. Their support post-implementation was unparalleled. I haven’t worked with a company more dedicated to their clients.",
        "caption": "Kyle Todt - Manager of Systems and Support, Royal College of Dentists of Canada",
        "clientName": "rcdc"
      },
      {
        "caption": "Murray Cuff - CEO, The Housse",
        "quote": "Quite possibly the brightest, most capable people I have worked with: tireless, hard-working, and patient.', //They communicate effectively and embrace limitless possibilities, while always getting the job done.",
        "clientName": "housse"
      },
      {
        "caption": "Sarah McCoubrey - Executive Director, Ontario Justice Education Network",
        "quote": "The experience exceeded our expectations: on budget and with attention to our culture and challenges. As we contemplated a big change, it is a relief to get professional, high quality help.",
        "clientName": "ojen"
      },
      {
        "caption": "Sandy Houston - CEO, Metcalf Foundation",
        "quote": "We gave this group a challenging and complex problem. We received an elegant and comprehensive response. Their work was strong, timely and effective. They're a thoughtful and creative team.",
        "clientName": "metcalf"
      },
      {
        "quote": "The Mushroom Cloud team helped develop our system for screening grant applications and provided valuable guidance to NGO's in our portfolio when they missed performance targets. Conscientious, creative and reliable.",
        "caption": "Moshe Schapiro - CEO, Friedberg Foundation",
        "clientName": "friedberg"
      }
    ];

  }
  renderTestimonials() {
    return this.getTestimonials().map((testimonial) => (
      <Testimonial key={testimonial.clientName} testimonial={testimonial} />
    ));
  }
  render() {
    return (<div>
      Testimonials !
      {this.renderTestimonials()}
    </div>);
  }
}

export default Testimonials;
