import React from 'react';
import PersonTeaser from './PersonTeaser';

class OurTeam extends React.Component {
  getPersonTeasers() {
    return [
      {
        "name": "Daniel Bernhard",
        "title": "President",
        "imgSrc": "/images/headshot-daniel.jpg",
        "_id": "daniel"
      },
      {
        "name": "Dr. Gregory Hall",
        "title": "Strategy & Operations",
        "imgSrc": "/images/headshot-greg.jpg",
        "_id": "greg"
      },
      {
        "name": "Eva Majernikova, PhD",
        "title": "Research & Engagement",
        "imgSrc": "/images/headshot-eva.jpg",
        "_id": "eva"
      },
      {
        "name": "George Carothers, PhD",
        "title": "Research & Engagement",
        "imgSrc": "/images/headshot-george.jpg",
        "_id": "george"
      },
      {
        "name": "Zack Newsham",
        "title": "Technology",
        "imgSrc": "/images/headshot-zack.jpg",
        "_id": "zack"
      },
      {
        "name": "Oni Ornan",
        "title": "Quantitative Analysis",
        "imgSrc": "/images/headshot-oni.jpg",
        "_id": "oni"
      },
      {
        "name": "Jan-Willem van de Meent, PhD",
        "title": "Data Master",
        "imgSrc": "/images/headshot-jw.jpg",
        "_id": "jw"
      }
    ];
  }
  renderPersonTeasers() {
    return this.getPersonTeasers().map((person) => (
      <PersonTeaser key={person._id} person={person} />
    ));
  }
  render() {
    return (<div>
      <h3> OurTeam ! </h3>
      {this.renderPersonTeasers()}
    </div>);
  }
}

export default OurTeam;
