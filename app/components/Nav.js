import  React from 'react';
import  { Link, IndexLink } from 'react-router';


class Nav extends React.Component {
  render() {
    return (<div>
      <ul>
        <li><IndexLink to="/" activeClassName="active">Home</IndexLink></li>
        <li><Link to="about" activeClassName="active">About</Link></li>
        <li><Link to="blog" activeClassName="active">Blog</Link></li>
        <li><Link to="case-studies" activeClassName="active">CaseStudies</Link></li>
        <li><Link to="contact" activeClassName="active">Contact</Link></li>
        <li><Link to="services" activeClassName="active">Services</Link></li>
      </ul>
    </div>);
  }
}

export default Nav;
