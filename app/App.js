import  React from 'react';
import  Nav from './components/Nav';
import  Footer from './components/Footer';

class App extends React.Component {
  render() {
    return (<div>
      <h4><Nav/></h4>

      {this.props.children}

      <Footer/>
      </div>);
  }
}

export default App;
