import React from 'react';

import SiteMap from './SiteMap';

class Footer extends React.Component {
  render() {
    return (<div>
      <hr/>

        <SiteMap/>

      <hr/>

      <h4>© 2017 MUSHROOM CLOUD. ALL RIGHTS RESERVED</h4>

      </div>);
  }
}

export default Footer;
