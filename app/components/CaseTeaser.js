import React from 'react';
import {Link} from 'react-router'

class CaseTeaser extends React.Component {
  render() {
    return (<div>
      <h4><Link to={'case/' + this.props.c._id}>{this.props.c.clientName}</Link></h4>
      <h5>{this.props.c.summary}</h5>
    </div>);
  }
}

export default CaseTeaser;
