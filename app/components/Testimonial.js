import React from 'react';
import {Link} from 'react-router'

class Testimonial extends React.Component {
  render() {
    return (<div>
      <h4>{this.props.testimonial.quote}</h4>
      <h5>{this.props.testimonial.caption}</h5>
    </div>);
  }
}

export default Testimonial;
