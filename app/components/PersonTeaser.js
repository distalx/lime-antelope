import React from 'react';
import {Link} from 'react-router'

class PersonTeaser extends React.Component {
  render() {
    return (<div>
      <h4><Link to={'team/' + this.props.person._id}>{this.props.person.name}</Link></h4>
      <h5>{this.props.person.title}</h5>
    </div>);
  }
}

export default PersonTeaser;
