import React from 'react';


class Case extends React.Component {
  getCaseInfo(caseId) {
    let cases = [
          {
            "id": "rcdc",
            "subject": "Harmonizing and digitizing national certification exams",
            "logo": "/images/logo-RCDC.png",
            "logoMaxWidth": "275px",
            "client": "Royal College of Dentists of Canada",
            "industry": "Education",
            "services": "Technology, Operations",
            "testimonial": "Mushroom Cloud quickly learned our business and...significantly improved our processes... I haven’t worked with a company more dedicated to their clients.",
            "testimonialAuthor": "Kyle Todt - Manager of Systems and Support, RCDC",
            "studySection": [{
            	"title": "The Challenge",
            	"descriptionAboveImage": [
            		"RCDC exams are a high-stakes affair. Pass, and you're setup for a lucrative career. Fail, and your expensive education is basically worthless. RCDC approached us with a desire to create standard difficulty measures across specialties. If they could prove mathematically that each test is of equivalent difficulty to the others, they could explain pass-fail rates with precision and defend against expensive, time-consuming appeals by failed candidates."
            	],
            	"image": {
            		"path": "/images/rcdc-exam.png",
            		"position": "left",
            		"caption": "Specialists administering an oral examination"
            	},
            	"descriptionBelowImage": []
            }, {
            	"title": "Our Approach",
            	"descriptionAboveImage": [
            		"Our basic plan was to harmonize and digitize. We began by mapping the exams for each of the 11 specialties onto a common, uniform structure. Once this was done, we produced a secure, web-based platform where volunteers could create exam content and create exams from the saved content. We also provided clear, intuitive insturctional materials to help examiners get up to speed with the new system."
            	],
            	"image": {
            		"path": "/images/oskar-case-eb.png",
            		"position": "right",
            		"caption": "Instructional materials"
            	},
            	"descriptionBelowImage": []
            }, {
            	"title": "Results",
            	"descriptionAboveImage": [
            		"The system has now been used to create thousands of exams. Each exam can be rigorously assessed using a powerful statistical engine. This ensures that all results are valid and greatly simplifies RCDC's ability to respond to appeals."
            	],
            	"image": {
            		"path": "/images/oskar-case-kv.png",
            		"position": "left",
            		"caption": "Statistical validation engine to clean and validate results"
            	},
            	"descriptionBelowImage": []
            }],
            "pun": "Want to drill down to the root of the problem?"
            }, {
            "id": "equestrian-canada",
            "subject": "New process, new technology, better service.",
            "logo": "/images/EC_logo.png",
            "client": "Equestrian Canada",
            "industry": "Sports",
            "services": "Organizational Development, Technology, Stragegy, Stakeholder Engagement",
            "testimonial": "",
            "testimonialAuthor": "Eva Havaris - CEO, Equestrian Canada",
            "studySection": [{
            	"title": "The Challenge",
            	"descriptionAboveImage": [
            		"In 2015, Equestrian Canada's new management committed to make the administrative side of their organization perform at the highest possible level, just like the athletes EC serves. EC called on Mushroom Cloud to make comprehensive improvements to its core member service processes and any related technology. This is exactly the type of strategy + operations + technology project we excel at, so the team was super excited to dive in."
            	],
            	"image": {
            		"path": "/images/ec-eventing.jpg",
            		"position": "right",
            		"caption": ""
            	},
            	"descriptionBelowImage": []
            }, {
            	"title": "Our Approach",
            	"descriptionAboveImage": [
            		"We started with a review of the business functions that affect the largest number of members, and settled on two priority areas: competitions governance and access to information.",
            		"On the competitions side, we began with a thorough review of the existing process, identifying gaps and pain points along the way. We then studied best-in-class Equestrian Federations from around the world, as well as EC's conunterparts governing other sports in Canada. We also conducted a wide-ranging stakeholder consultation exercise that engaged athletes, coaches, provincial officials, and other stakeholders across Canada."
            	],
            	"image": {
            		"path": "/images/ec-process-map.jpg",
            		"position": "left",
            		"caption": "Putting together a process map"
            	},
            	"descriptionBelowImage": [
            		"To deal with the information sharing issue, we set out to build a highly functional website that would make it easy for people to find the information they need, in English or French, on any device, without having to call in for help. We also wanted to build infrastructure that could accommodate more complex tasks that will be added in future. Therefore we set out to fully integrate the website with the member database, creating a dynamic, engaging experience built upon a solid structure."
            	]
            }, {
            	"title": "Results",
            	"descriptionAboveImage": [
            		"The competitions review is in the early phases of implementation, with changes to be rolled out gradually by the end of the 2017 competition season. The new web platform launched in July 2016, just in time for the Rio Olympics. The core feature is a full text \"finder\", that lets you find anything on the site, from anywhere on the site.."
            	],
            	"descriptionBelowImage": []
            }],
            "resultVideo": {
            	"poster": "/images/ec-home-after.png",
            	"mp4": "/images/ec-search-video.mp4",
            	"ogv": "/images/ec-search-video.ogv"
            },
            "beforeAfter": [{
            		"beforeImage": "/images/ec-home-before.png",
            		"beforeImageCaption": "The old site was tough to navigate and not mobile friendly",
            		"afterImage": "/images/ec-home-after.png",
            		"afterImageCaption": "The new site is clear, informative, and perfect on all devices."
            	},

            	{
            		"beforeImage": "/images/ec-docs-before.png",
            		"beforeImageCaption": "The old site forced people to dig through thousands of documents to find what they were looking for.",
            		"afterImage": "/images/ec-finder.png",
            		"afterImageCaption": "On the new site, you can find anything on the site from anywhere on the site with realtime search."
            	}

            ],
            "pun": "What does better look like for you?"
            }, {
            "id": "metcalf-platform",
            "subject": "A Shared Charitable Platform for the Arts",
            "logo": "/images/logo-metcalf-foundation.png",
            "logoMaxWidth": "",
            "client": "George Cedric Metcalf Foundation",
            "industry": "Philanthropy, Arts",
            "services": "Strategy, Modelling, Research, Engagement",
            "testimonial": "We gave this group a challenging and complex problem. We received an elegant and comprehensive response. Their work was strong, timely and effective. They're a thoughtful and creative team.",
            "testimonialAuthor": "Sandy Houston - CEO, Metcalf Foundation",
            "studySection": [{
            	"title": "The Challenge",
            	"descriptionAboveImage": [
            		"As one of Canada's leaders in philanthropy, <a href=\"http://metcalffoundation.com\" target=\"_blank\"> The George Cedric Metcalf Foundation</a> has been leading a multi-year effort to establish a Shared Charitable Platform that would allow small-scale independent artists to share the cost of professional adminstration, issue charitable tax receipts, and foster new collaborations. Having previously commissioned an abstract report on the subject, Metcalf turned to us to work out the practical details that would bring the platform to life: What would be required to set up such a structure? How much it would cost to establish? What would the price of membership need to be for the platform to be sustainable? Would artists be willing or able to join at this price? We set off to find out."
            	],
            	"image": {
            		"path": "/images/tpc.jpg",
            		"position": "right",
            		"caption": "Prospective platform members, <a href=\"http://www.thetoypianocomposers.com/\" target=\"_blank\">The Toy Piano Composers"
            	},
            	"descriptionBelowImage": []
            }, {
            	"title": "Our Approach",
            	"descriptionAboveImage": [
            		"We began with a study of similar structures in Canada and the United States. Based on what we learned, we produced a dynamic 3-year financial model that projects the cost of maintaining the platform according to the size and composition of the membership in three probable scenarios. From these models, we deduced an approximate membership price. We followed the modelling effort with a comprehensive survey of the target audience, processing responses into a value assessment comparing current conditions to those under the Platform. Finally, with all the evidence before us, we weighed the various options, risks, and opportunities, to make a recommendation and craft a realistic implementation plan."
            	],
            	"image": {
            		"path": "/images/platform-survey-sample.png",
            		"position": "left",
            		"caption": "Sample of artists who contributed to the design process"
            	},
            	"descriptionBelowImage": []
            }, {
            	"title": "Results",
            	"descriptionAboveImage": [
            		"The Metcalf Foundation used our work to convince the country's leading arts funders to finance a pilot project together. This consortium includes the <a href=\"http://canadacouncil.ca/\" target=\"_blank\">Canada Council for the Arts</a>, the <a href=\"http://www.arts.on.ca/\" target=\"_blank\">Ontario Arts Council</a>, the <a href=\"http://www.torontoartscouncil.org/home\" target=\"_blank\">Toronto Arts Council</a>, and the <a href=\"http://otf.ca\" target=\"_blank\">Ontario Trillium Foundation</a>. The consortium pledged $600,000 for this project and is currently solidifying the regulatory framework in which it can operate successfully."
            	],
            	"descriptionBelowImage": []
            }],
            "resultImage": {
            	"path": "/images/armstrongs-war.jpg",
            	"caption": "Scene from <strong>Armstrong's War</strong> by independent playwright, GG Laureate, and platform supporter, <a href=\"http://colleenmurphy.ca/\" target=\"_blank\">Colleen Murphy</a>"
            },
            "pun": "Want to learn more about this project, or discuss how we could do something similar for you?"
            }, {
            "id": "otf",
            "subject": "Evaluating the Economic Impact of Community Grants",
            "logo": "/images/logo-ontario-trillium.jpg",
            "logoMaxWidth": "",
            "client": "Ontario Trillium Foundation",
            "industry": "Philanthropy",
            "services": "Modelling, Evaluation, Systems Development",
            "testimonial": "",
            "testimonialAuthor": "Doug Gore - Strategy Lead (Active People), Ontario Trillium Foundation",
            "studySection": [{
            	"title": "The Challenge",
            	"descriptionAboveImage": [
            		"The Ontario Trillium Foundation is Canada's largest grant-maker and a global leader pushing the envelope of philanthropic best practice. They approached us with a juicy, complex problem: how to measure the economic impact of the $120 million they invest in Ontario communities each year."
            	],
            	"descriptionBelowImage": []
            }, {
            	"title": "Our Approach",
            	"descriptionAboveImage": [
            		"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam quis risus eget urna mollis ornare vel eu leo. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Nulla vitae elit libero, a pharetra augue. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec ullamcorper nulla non metus auctor fringilla. Nulla vitae elit libero, a pharetra augue. Donec ullamcorper nulla non metus aucto."
            	],
            	"descriptionBelowImage": []
            }],
            "pun": "Got a juicy challenge to discuss?"
            }, {
            "id": "sask-board",
            "subject": "Grants Management System and Data Migration",
            "logoMaxWidth": "",
            "logo": "/images/logo-sask-arts-board.jpg",
            "client": "Saskatchewan Arts Board",
            "industry": "Arts",
            "services": "Web development, legacy systems migration",
            "testimonial": "Mushroom Cloud was incredibly helpful – helping us envision solutions that we hadn’t even imagined, working with partners around the globe to meet our unusual needs, and delivering the project on time and budget. We couldn’t have been more pleased.",
            "testimonialAuthor": "Michael Jones - CEO, Saskatchewan Arts Board",
            "studySection": [{
            	"title": "The Challenge",
            	"description": "For many years, The Saskatchewan Arts Board had tracked grant applications and awards in a database running on DOS machine. The system could only run on an antiquated computer that was literally falling apart, and all the data were trapped inside. Our job was to pull it out and move it to a modern, web-based platform that could later be expanded upon.",
            	"image": {
            		"path": "/images/sask-board-before.jpg",
            		"position": "right",
            		"caption": "The old database running on DOS"
            	},
            	"descriptionBelowImage": []
            }, {
            	"title": "Our Approach",
            	"descriptionAboveImage": [
            		"Working with a legacy systems expert in New Zealand, we wrote a script to pull the data from the DOS machine and convert it into a modern database. This involved quite a bit of trial and error as there were hundreds of thousands of records and a great deal of variation between them."
            	],
            	"image": {
            		"path": "/images/sask-migration-script.png",
            		"position": "left",
            		"caption": "Part of the data migration script"
            	},
            	"descriptionBelowImage": [
            		"To provide the best possible value, we took great pains to future-proof the design of the new system so that the Board can easily add functionality later on, without having to start from scratch. Therefore the system we deilvered is mostly ready to accept grant applications online, and it comes with a powerful reporting engine that can support larger, more complex queries."
            	]
            }, {
            	"title": "The Result",
            	"descriptionAboveImage": [
            		"We delivered a system that is fast, secure, reliable, and modern in every sense of the term. It uses the latest technologies, it is mobile friendly, and it is extremely easy to maintain. Most importantly, it relieves the Saskatchewan Arts Board of a significant operating liability; their data are now safe for many years to come."
            	],
            	"descriptionBelowImage": []
            }],
            "resultImage": {
            	"path": "/images/sask-board-grant.png",
            	"caption": "What a grant record looks like in the new system"
            },
            "pun": "Time to transition off of antiquated technology?"
          }
        ];

        return cases.find((c)=>{
          return c.id === caseId;
        })
  }
  renderCase(caseId) {
    let c = this.getCaseInfo(caseId)


    return (<div>
      <h3>{c.client}</h3>
      <h4>{c.subject}</h4>
      <h5>{c.services}</h5>
    </div>)
  }
  render() {
    return (<div>
      <h3>Case !</h3>
      {this.renderCase(this.props.params.caseId)}
    </div>);
  }
}

export default Case;
