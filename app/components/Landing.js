import React from 'react';

import Clientele from './Clientele';
import InteractiveCTA from './InteractiveCTA';
import Testimonials from './Testimonials';
import WhatWeDo from './WhatWeDo';



class Landing extends React.Component {
  render() {
    return (<div>
      <hr/>
      Landing !
      <hr/>
      <WhatWeDo/>
      <hr/>
      <Clientele/>
      <hr/>
      <Testimonials/>
      <hr/>
      <InteractiveCTA/>
      <hr/>
    </div>);
  }
}

export default Landing;
