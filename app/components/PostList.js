import React from 'react';

import PostTeaser from './PostTeaser';

class PostList extends React.Component {
  getPostTeasers() {
    return [
      { _id: 1, title: 'Post 1', body:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut " },
      { _id: 2, title: 'Post 2', body:"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem." },
      { _id: 3, title: 'Post 3', body:"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae" },
    ];
  }
  renderPostTeasers() {
    return this.getPostTeasers().map((post) => (
      <PostTeaser key={post._id} post={post} />
    ));
  }
  render() {
    return (<div>
      <h3> PostList ! </h3>
      {this.renderPostTeasers()}
    </div>);
  }
}

export default PostList;
