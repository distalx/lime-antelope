import React from 'react';

import CaseTeaser from './CaseTeaser';

class CaseList extends React.Component {
  getCaseTeasers() {
    return [
      {
        "clientName": "Equestrian Canada",
        "summary": "Equestrian Canada supports the incredible athletes - both human and equine - who put Canada on the podium year in and year out. EC teamed up with Mushroom Cloud to completely overhaul its member service practices and technology infrastructure.",
        "_id": "equestrian-canada",
        "clientClass": "equestrian-canada",
        "scrollRevealDeets": "enter left and move 80px over 0.8s",
        "tags": ["Operations", "Technology"],
        "captionText": "Ian Millar and Star Power"
      },
      {
        "clientName": "Saskatchewan Arts Board",
        "summary": "The Saskatchewan Arts Board supports a thriving ecosystem of artists and arts organizations with about $7 million in annual grants. How do they keep track of all these grants? They used to use an antiquated system running on DOS (remember DOS?) that was literally falling apart, posing a huge risk to the organization. We rescued 20 years of data and moved them to a secure, reliable, web-based platform.",
        "_id": "sask-board",
        "clientClass": "sask-board",
        "scrollRevealDeets": "enter right and move 80px over 0.8s",
        "tags": ["Technology"],
        "captionText": "Work by Ovide Bighetty displayed in Outlook, Saskatchewan"
      },
      {
        "clientName": "Metcalf Foundation",
        "summary": "It isn't easy for small-scale independent artists to practice their craft. Money is tight, public funding isn't keeping up with demand, and the cost of maintaining charitable status is prohibitive. Shared Charitable Platforms provide a structure that independent projects can join, complete with tax exempt status and capable, professional administration.",
        "_id": "metcalf-platform",
        "clientClass": "metcalf",
        "scrollRevealDeets": "enter left and move 80px over 0.8s",
        "tags": ["Consulting"],
        "captionText": "Sealed Angel, by Soundstreams (Metcalf grantee)"
      },
      {
        "clientName": "Royal College of Dentists of Canada",
        "summary": "The Royal College of Dentists of Canada certifies all dental specialists, including orthodontists, pediatric dentists, and oral surgeons. They wanted to bring their examinations online and create a powerful statistical engine to analyze and validate results, so that each of us can rest assured that the hands behind the drill are steady and well-practised.",
        "_id": "rcdc",
        "clientClass": "rcdc",
        "scrollRevealDeets": "enter right and move 80px over 0.8s",
        "tags": ["Operations", "Technology"],
        "captionText": "Photo credit: Canadian Business"
      }
    ];

  }
  renderCaseTeasers() {
    return this.getCaseTeasers().map((c) => (
      <CaseTeaser key={c._id} c={c} />
    ));
  }
  render() {
    return (<div>
      <h3> CaseList ! </h3>
      {this.renderCaseTeasers()}
    </div>);
  }
}

export default CaseList;
