import  React from 'react';

import OurTeam from './OurTeam';

class About extends React.Component {
  render() {
    return (<div>
      <h2>About !</h2>

      <OurTeam/>

    </div>);
  }
}

export default About;
