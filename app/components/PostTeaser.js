import React from 'react';
import {Link} from 'react-router'

class PostTeaser extends React.Component {
  render() {
    return (<div>
      <h4><Link to={'post/' + this.props.post._id}>{this.props.post.title}</Link></h4>
      <h5>{this.props.post.body}</h5>
    </div>);
  }
}

export default PostTeaser;
