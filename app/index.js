import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import App from './App';
import About from './components/About';
import Blog from './components/Blog';
import Case from './components/Case';
import CaseStudies from './components/CaseStudies';
import Contact from './components/Contact';
import Landing from './components/Landing';
import Post from './components/Post';
import Person from './components/Person';
import Services from './components/Services';



ReactDOM.render(
  (<Router  history={browserHistory}>
      <Route path="/" component={App}>
        <Route path="about" component={About}/>
        <Route path="blog" component={Blog}/>
        <Route path="/case/:caseId" component={Case}/>
        <Route path="/post/:postId" component={Post}/>
        <Route path="/team/:personId" component={Person}/>
        <Route path="contact" component={Contact}/>
        <Route path="case-studies" component={CaseStudies}/>
        <Route path="services" component={Services}/>
        <IndexRoute component={Landing}/>
      </Route>
  </Router>),
  document.getElementById('app')
);
